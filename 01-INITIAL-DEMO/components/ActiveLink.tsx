import Link from "next/link"
import { useRouter } from "next/router"
import React from "react";
import { CSSProperties, FC } from 'react';
// ESte codigo CSS se deja fuera del componente para que no se re procese cada vez que se renderice el componente.

const style: CSSProperties = {
    color: '#0070f3',
    textDecoration: 'underline'
}

interface Props{
    text:string;
    href:string;
}


export const ActiveLink:FC<Props> = ({ text, href }) => {

    const { asPath } = useRouter();

    return (
        <Link href={href}>
            <span style={asPath === href ? style : undefined}>{text}</span>
        </Link>

    )
}
