import { MainLayout } from "../../components/layouts/MainLayout";
import Link from 'next/link'


const index = () => {
    return (
        <MainLayout>
          <h1>Pricing Page</h1>
    
          <h1 className={'title'}>
            {/* El anchor tag funciona perfectamente pero tiene que recargar toda la pagia de transicion en transicion */}
            {/* Ir a <a href="/about">About</a>  */}
    
            {/* El componente Link  le dice a NEXTJS que se haga un pre fetch de la siguiente pagina, para que se cargue desde el inicio y la transicion sea mas rapida*/}
            Ir a <Link href="/" >Home</Link>
          </h1>
    
          <p className={'description'}>
            Get started by editing{' '}
            <code className={'code'}>pages/pricing/index.jsx</code>
          </p>
        </MainLayout>
      )
}

export default index;