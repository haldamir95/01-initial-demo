/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  /** Esta configuracion se agrega para el correcto levantamiento de la imagen de docker con el error 
   * de stand alone
   */
  experimental:{
    outputStandalone: true
  }
}

module.exports = nextConfig
