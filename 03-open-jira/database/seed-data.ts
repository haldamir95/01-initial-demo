interface SeedData {
    entries: SeedEntry[];
}

interface SeedEntry {
    description: string;
    status: string;
    createdAt: number;
}



export const seedData:SeedData = {
    entries: [
        {
            description: 'pending descripcion',
            status: 'pending',
            createdAt: Date.now()
        },
        {
            description: 'in progress description',
            status: 'in-progress',
            createdAt: Date.now() - 1000000
        },
        {
            description: 'finished descripcion',
            status: 'finished',
            createdAt: Date.now() - 100000
        },
    ]
}