import { createContext } from 'react';
import { Entry } from '../../Interfaces';

//es importante que en TypeScript definir que tipo de datos expone este Contexto, por eso creamos una interfaz
interface ContextProps {
    entries: Entry[];

    //Methods
    addNewEntry: (description: string) => void
    updateEntry: (entry: Entry) => void
}

export const EntriesContext = createContext({} as ContextProps);