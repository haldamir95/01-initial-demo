import { FC, useReducer, ReactNode, useEffect } from 'react';
import { EntriesContext, entriesReducer } from './';
import { Entry } from '../../Interfaces/entry';
import entriesApi from '../../apis/entriesApi';


interface Props {
    children:ReactNode
}

export interface EntriesState {
    entries: Entry[];
}

const Entries_INITIAL_STATE: EntriesState = {
    entries: [],
}

export const EntriesProvider:FC<Props> = ({ children }) => {

    //creamos el Reducer Inicial que nos asigna nuestros Stados iniciales
    const [state, dispatch] = useReducer(entriesReducer, Entries_INITIAL_STATE)

    //metodo para crear un nuevo entry que recibe unicamente la descripcion
    const addNewEntry = async (description:string) => {
        //nuevo Entry creado desde el frontend al inicio del curso
        // const newEntry: Entry = {
        //     _id: uuidv4(),
        //     description: description,
        //     createdAt: Date.now(),
        //     status: 'pending'
        // }

        
        const {data} = await entriesApi.post<Entry>('/entries',  {description: description})
        //Utilizamos el dispatch de nuestro entiresReducer para ejecutar la accion Add-Entry y le mandamos como payload la nueva Entry
        dispatch({type:'[Entries] Add-Entry', payload:data})
    }

    const updateEntry = async (entry:Entry) => {
        try{
            const { data } = await entriesApi.put<Entry>(`/entries/${entry._id}`, entry)
            dispatch({type:'[Entries] Entry-Updated', payload:data})
        }catch(error){
            console.log(error)
        }
    }


    //funcion para refrescar entradas
    const refreshEntries = async () => {
        const {data} = await entriesApi.get<Entry[]>('/entries')
        dispatch({ type: '[Entries] Refresh-Data', payload:data})
    }
    //utilizar useEffect para traer toda la informacion del servidor
    useEffect(() => {
        refreshEntries()
    }, [])
    

    return (
        <EntriesContext.Provider value={{
            ...state,

            //Methods
            addNewEntry,
            updateEntry,
        }}>
            { children }
        </EntriesContext.Provider>
  )
}