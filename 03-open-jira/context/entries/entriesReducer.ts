import { Entry } from '../../Interfaces';
import { EntriesState } from './';


type EntriesActionType = 
| { type: '[Entries] Add-Entry', payload: Entry } //accion para agregar y en el payload viene la entrada nueva
| { type: '[Entries] Entry-Updated', payload: Entry } //accion para agregar y en el payload viene la entrada nueva
| { type: '[Entries] Refresh-Data', payload: Entry[] } 

export const entriesReducer = ( state:EntriesState, action:EntriesActionType ):EntriesState =>{

    switch (action.type) {
        case '[Entries] Add-Entry':
            return {
                ...state,//siempre se tiene que retornar el state nuevo
                entries: [...state.entries, action.payload]//actualizamos la propiedad entries del state en la cual le copiamos todas las entries originale sy agregamos el entry del payload
                
            }
        case '[Entries] Entry-Updated':
            return {
                ...state,
                entries: state.entries.map( entry => {//iteramos entre las entradas
                    if(entry._id === action.payload._id){//buscamos el id de la tarjeta a modificar
                        entry.status = action.payload.status//cambiamos estatus
                        entry.description = action.payload.description//cambiamos descripcion
                    }
                    return entry//regornamos la tarjeta modificada
                })
            }
            case '[Entries] Refresh-Data':
                return{
                    ...state,
                    entries: [...action.payload]
                }
        default:
            return state
    }
}