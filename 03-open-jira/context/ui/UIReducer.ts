//3
import { UIState } from "./"; //importamos el STATE TYPE que declaramos en el Provider

//Declaramos especificamente los tipos de ACTIONS que podemos recibir
type UIActionType =
  | { type: "UI - Open Sidebar" }
  | { type: "UI - Close Sidebar" }
  | { type: "UI - Set isAddingEntry"; paylod: boolean } //accion que va a manejar el toggle del formulario de para una nueva entry
  | { type: "UI - Start Dragging" } //accion que va a manejar el inicio del dragging
  | { type: "UI - End Dragging" } //accion que va a manejar el fin del dragging

//Funcion reducer que retorna el STATE TYPE y recibe un STATE TYPE y tambien un ACTION TYPE
export const UIReducer = (state: UIState, action: UIActionType): UIState => {
  //evaluando cada ACTION TYPE para crear un nuevo STATE con algun atributo modificado
  switch (action.type) {
    case "UI - Open Sidebar":
      return {
        ...state, //se hace un spread del STATE original para que se sepa que el STATE es nuevo
        sidemenuOpen: true,
      };
    case "UI - Close Sidebar":
      return {
        ...state,
        sidemenuOpen: false,
      };
    case "UI - Set isAddingEntry":
      return {
        ...state,
        isAddingEntry: action.paylod,
      };
    case "UI - Start Dragging":
      return {
        ...state,
        isDragging: true,
      };
    case "UI - End Dragging":
      return {
        ...state,
        isDragging: false,
      };
    default:
      return state; //si no entro a ningun ACTION TYPE se retorna solo el state para que no se tome como un cambio de STATE
  }
};
