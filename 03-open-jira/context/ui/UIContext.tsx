//1
import { createContext } from 'react';

//es importante que en TypeScript definir que tipo de datos expone este Contexto, por eso creamos una interfaz
interface ContextProps {
    sidemenuOpen: boolean;
    isAddingEntry:boolean;
    isDragging:boolean;

    //METHODS
    //agregar los metodos que va a retornar el <UIContext.Provider>
    openSideMenu: () => void;
    closeSideMenu: () => void;
    setIsAddingEntry: (isAddingEntry: boolean) => void;
    startDragging: () => void;
    endDragging: () => void;
}

export const UIContext = createContext({} as ContextProps); 