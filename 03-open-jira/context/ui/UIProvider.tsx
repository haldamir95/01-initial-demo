//2
import { FC, useReducer, ReactNode } from 'react';
import { UIContext, UIReducer } from './';

//Interfaz para los props del componente
interface Props {
    children:ReactNode
}

//Interfaz que define el objeto ESTADO que vamos a manejar
export interface UIState {
    sidemenuOpen: boolean;
    isAddingEntry:boolean; //Agregamos el atributo para el manejo del toggle del menu de nuevo entry
    isDragging: boolean; //propiedad para saber si un componente esta dragging y cambiarle el aspecto
}

//Objeto con los valores iniciales de nuestro ESTADO
const UI_INITIAL_STATE: UIState = {
    sidemenuOpen: false,
    isAddingEntry: false, //seteamos el valor inicial del atributo
    isDragging: false,
}

//Componente
export const UIProvider:FC<Props> = ({ children }) => {

    //useReducer para poder ejecutar los dispatch y cambiar los valores del ESTADO
    const [state, dispatch] = useReducer(UIReducer, UI_INITIAL_STATE)


    //implementar las acciones del REDUCER para ejecutar el cambio de STATE
    const openSideMenu = () => {
        dispatch({ type: 'UI - Open Sidebar' })
    }

    const closeSideMenu = () => {
        dispatch( { type: 'UI - Close Sidebar' } )
    }


    //Crear metodo que utiliza el dispatch de nuestro Reducer para cambiar el estado del atributo isAddingEntry
    const setIsAddingEntry = (isAddingEntry:boolean) => {
        dispatch( {type:'UI - Set isAddingEntry', paylod:isAddingEntry} )
    }

    //crear el dispat para el dragging
    const startDragging = () => {
        dispatch({type:'UI - Start Dragging'})
    }

    const endDragging = () => {
        dispatch({type:'UI - End Dragging'})
    }

    //Componente del Contexto con los valores del ESTADO
    return (
        <UIContext.Provider value={{
            //VALUES
            //sidemenuOpen: state.sidemenuOpen
            //desestructuramos el state del reducer para que se tomen todos los valores que trae dentro
            ...state,

            //METHODS
            //retornamos tambien la funcion para que todos los demas compoentes hijos puedan utilizar la funcion
            openSideMenu,
            closeSideMenu,
            setIsAddingEntry,
            startDragging,
            endDragging,
        }}>
            {/* Retornando todos los hijos (nodos que comparten el eESTADO) */}
            { children }
        </UIContext.Provider>
  )
}