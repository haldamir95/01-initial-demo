import { createTheme, AppBar } from '@mui/material';
import { grey, red } from "@mui/material/colors";

export const lightTheme = createTheme({
    palette: {
      mode:'light',
      background:{
        default: grey[300]
      },
      primary:{
        main:'#4a148c'
      },
      secondary:{
        main:'#19857b'
      },
      error:{
        main: red.A400
      }
    },
    
    components:{
      MuiAppBar:{
        defaultProps: {
          //Elevation agrega un shadow debajo del componente para que paresta que esta elevado 0 - 24
          elevation: 0
        },
        styleOverrides:{
          
        }
      }
    }
  });