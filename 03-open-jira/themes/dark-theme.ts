import { createTheme } from "@mui/material";
import { red } from "@mui/material/colors";

export const darkTheme = createTheme({
  palette: {
    mode:'dark',
    secondary:{
      main:'#19857b'
    },
    error:{
      main: red.A400
    }
  },
  
  components:{
    //Cambiar la configuracion de un componente ya existente, en este caso el AppBar
    MuiAppBar:{
      defaultProps: {
        //Elevation agrega un shadow debajo del componente para que paresta que esta elevado 0 - 24
        elevation: 0
      },
      styleOverrides:{
        root:{
          backgroundColor: '#4a148c'
        }
      }
    }
  }
  });