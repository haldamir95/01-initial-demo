import { FC, useContext } from 'react';
import { AppBar, IconButton, Toolbar, Typography } from "@mui/material";
import MenuOutlinedIcon from '@mui/icons-material/MenuOutlined';
import { UIContext } from '../../context/ui';


export const Navbar:FC = () => {

  const { openSideMenu } = useContext( UIContext )

  return (
    <AppBar position='sticky' >
        <Toolbar>
            {/* Para los Icons podemos ir a la pagina oficial de Material y buscar los Icons https://mui.com/material-ui/icons/ */}
            <IconButton 
                size='large'
                edge='start'
                onClick={ openSideMenu }
            >
                <MenuOutlinedIcon/>
            </IconButton>
            <Typography variant="h6">OpenJira</Typography>
        </Toolbar>

    </AppBar>
  )
}
