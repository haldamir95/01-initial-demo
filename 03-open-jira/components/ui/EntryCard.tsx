import { Card, CardActionArea, CardActions, CardContent, Typography } from '@mui/material'
import React, { DragEvent, FC, useContext } from 'react'
import { Entry } from '../../Interfaces'
import { UIContext } from '../../context/ui/UIContext';


interface Props {
    entry: Entry
}

export const EntryCard:FC<Props> = ({ entry }) => {

    const { startDragging, endDragging} = useContext(UIContext)


    const onDragStart = (event:DragEvent<HTMLDivElement>) => {
        //modificar el estado para indicar que estoy hacinedo drag
        event.dataTransfer.setData('text',entry._id) //el dataTransfer sirve para establecer informacion
        startDragging()
    }

    const onDragEnd = () => {
        endDragging()
    }

  return (
    
    <Card
        sx={{ marginBottom:1}}

        //aqui se agregaran eventos de drag and drop etc
        draggable
        onDragStart={onDragStart}
        onDragEnd={onDragEnd}
    >
        {/* Card Action Area funciona para que el card brille en el moseover y al hacerle click */}
        <CardActionArea>
            <CardContent>
                {/* whiteSpace sirve para que que los saltos de linea funcionen en los text areas */}
                <Typography sx={{ whiteSpace: 'pre-line' }}> 
                    {entry.description}
                </Typography>
                <CardActions sx={{ display:'flex', justifyContent:'end', paddingRight:2}}>
                    <Typography variant='body2'>
                        hace 30 min
                    </Typography>
                </CardActions>
            </CardContent>
        </CardActionArea>
    </Card>
  )
}
