import { Button, TextField } from "@mui/material";
import { SaveOutlined } from "@mui/icons-material";
import React, { ChangeEvent, useState } from "react";
import { Box } from "@mui/system";
import { AddCircleOutlineOutlined } from "@mui/icons-material";
import { useContext } from 'react';
import { EntriesContext } from '../../context/entries/EntriesContext';
import { UIContext } from "../../context/ui";

export const NewEntry = () => {
  //const [isAdding, setIsAdding] = useState(false);
  const [inputValue, setInputValue] = useState('')
  const [touched, setTouched] = useState(false)

  //utilizamos el useContext para desestructurar el metodo addNewEntry que proviene de nuestro EntriesContext
  const { addNewEntry } = useContext(EntriesContext)

  const { isAddingEntry, setIsAddingEntry } = useContext(UIContext)


  const onTextFieldChanged = (event:ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    setInputValue(event.target.value)
  }

  const onSave = () => {
    if( inputValue.length === 0) return
    addNewEntry(inputValue)  
    setIsAddingEntry(false)
    setTouched(false)
    setInputValue('')  
  }

  return (
    <Box sx={{ marginBottom: 2, padding: 2 }}>
      {isAddingEntry ? (
        <>
          <TextField
            fullWidth
            sx={{ marginTop: 2, marginBottom: 1 }}
            placeholder="Nueva Entrada"
            multiline
            label="Nueva Entrada"
            helperText={inputValue.length <=0 && touched && 'Ingrese un valor'}
            error={ inputValue.length <=0 && touched }
            value={inputValue}
            onChange={onTextFieldChanged}
            onBlur={ () => setTouched(true) }
          />

          <Box display="flex" justifyContent="space-between">
            <Button
              variant="outlined"
              color="secondary"
              endIcon={<SaveOutlined />}
              onClick={onSave}
            >
              Guardar
            </Button>

            <Button 
            variant="text"
            onClick={() => setIsAddingEntry(false)}
            >Cancelar</Button>
          </Box>
        </>
      ) : (
        <Button
          startIcon={<AddCircleOutlineOutlined />}
          fullWidth
          variant="outlined"
          onClick={() => setIsAddingEntry(true)}
        >
          Agregar Tarea
        </Button>
      )}
    </Box>
  );
};
