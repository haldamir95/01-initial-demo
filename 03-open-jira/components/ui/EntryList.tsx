import { List, Paper } from '@mui/material'
import { EntryCard } from './EntryCard'
import { EntryStatus } from '../../Interfaces/entry';
import { DragEvent, FC, ReactNode, useContext, useMemo } from 'react';
import { EntriesContext } from '../../context/entries/EntriesContext';
import { UIContext } from '../../context/ui';
import styles from './EntryList.module.css'


interface Props {
  status: EntryStatus
}


export const EntryList:FC<Props> = ({ status }) => {

  const {isDragging, endDragging} = useContext(UIContext)
  
  //usar useContext para empezar a utilizar nuestro contexto custom
  const { entries, updateEntry } = useContext(EntriesContext)
  //filtramos todas las entradas por status segun el que recibimos como argumento en los props del componente
    //utilizamos useMemo para memorizar las entries que no cambian siempre
      // param1: funcion que retorne un valor a memorizar
      // param2: arreglo de dependencias que dicen cuando se debe de volver a memorizar ese valor (como el arreglo del useEffect)
  const entriesByStatus = useMemo( () => entries.filter(entry => entry.status === status) , [entries])
  

  const allowDrop = (event:DragEvent<HTMLDivElement>) => {
    event.preventDefault(); //debido a que el comportamiento normal de un div es NO PERMITIR EL DROP, debemos de cancelar ese comportamiento para permitir hacer el DROP
  }

  const onDropEntry = (event:DragEvent<HTMLDivElement>) => {//creamos el metodo para manejar cuando se dropee una entry en la lista
    const id = event.dataTransfer.getData('text')
    
    //buscamos la entrada
    const entry = entries.find( e => e._id === id)!;//simbolo de admiracion para decir que siempre lo vamos a encontrar
    entry.status = status
    updateEntry(entry)
    endDragging()
  }

  return (
    //Aqui vamos a hacer el drop
    <div
      onDrop={onDropEntry}
      onDragOver={ allowDrop }
      className={isDragging? styles.dragging : ''}
    >
        <Paper sx={{ height: 'calc(100vh - 250px)', overflow:'auto', backgroundColor:'transparent', padding: "1px 5px" }}>

            {/* Cambiara opacidad cuando estamos haciendo drag o no */}
            {/* el transition -> modificamos TODAS las animaciones que tenga y les damos un delay de 0.3s */}
            <List sx={{ opacity: isDragging? 0.2 : 1, transition: 'all .3s'}}>
                {/* Barremos nuestro arrego entriesByStatus para listar todos los entries filtrados */}
                {
                  entriesByStatus.map( entry => (
                    <EntryCard key={entry._id} entry={entry}/>
                  ))
                }
            </List>
        </Paper>
    </div>
  )
}
