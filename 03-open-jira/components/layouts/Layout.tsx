import { Box } from "@mui/material";
import Head from "next/head";
import { FC, ReactNode } from "react";
import { Navbar, Sidebar } from "../ui";

interface Props{
    title?: string;
    children: ReactNode
}

export const Layout:FC<Props> = ({ title =  'Open Jira - App', children}) => {
  return (
    //sx prop es un prop que modifica el Style pero tiene acceso a los themes
    //flexFlow es para extender el componente lo mas posible
    <Box sx={{ flexFlow: 1 }}> 
        <Head>
            <title>{ title }</title>
        </Head>

        <Navbar></Navbar>

        <Sidebar></Sidebar>


        <Box sx={{ padding: '10px 20px' }}>
            { children }
        </Box>

    </Box>
  )
}
