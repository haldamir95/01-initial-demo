import mongoose, { Model, Schema } from 'mongoose'
import { Entry } from '../Interfaces'

//Extendemos una nueva interfaz de Entry para que tenga los mismos valores y se le puedan agregar mas para usos de base de datos
//hacer esto sirve para que el Model<IEntry> sepa cuales son los atributos que tiene mi objeto de tipo Model
export interface IEntry extends Entry {
}


//creamos un esquema
const entrySchema = new Schema({
    //en donde definiremos todos los atributos de nuestro esquema, los tipos y restricciones
    description: {type: String, required:true},
    createdAt: {type:Number },
    status:{
        type:String, //el status es de tipo string pero...
        enum:{
            values: ['pending','in-progress','finished'], //... solo puede tener estos valores
            message: '{VALUE} no es un estado permitido' //... y si se introduce un valor incorrecto nos meustra un error
        },
        default: 'pending' //establecer el status como 'pending' en caso de que no venga definido
    }
})


//para que los modelos no se repitan, si el modelo ya existe se le asigna el mismo y si no existe, se le envia el nuevo modelo con el schema
const EntryModel:Model<IEntry> = mongoose.models.Entry || mongoose.model('Entry', entrySchema)

export default EntryModel