// middleware.ts
import { NextResponse } from 'next/server'
import type { NextRequest } from 'next/server'

// This function can be marked `async` if using `await` inside
export function middleware(req: NextRequest) {
  
    if( req.nextUrl.pathname.startsWith('/api/entries/') ){
        const id = req.nextUrl.pathname.replace('/api/entries/','')//obtenemos el id del path
        
        const checkMongoIDRegExp = new RegExp("^[0-9a-fA-F]{24}$")//creamos un regexp para los ID de Mongo

        if(!checkMongoIDRegExp.test(id)){//verificamos si no hace match
            const url = req.nextUrl.clone()//clonamos el objeto de la url actual
            url.pathname = '/api/bad-request'//le cambiamos el path para que nos envie a la api bad-request
            url.search = `?message=${ id } is not a valid MongoID`
            return NextResponse.rewrite(url)
            
        }
    }
  return NextResponse.next()
}

// Configuracion para que el middleware se ejecute solo con la ruta
export const config = {
  matcher: [
        // '/api/:path',
        '/api/entries/:path*'
    ]
}