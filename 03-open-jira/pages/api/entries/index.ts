import type { NextApiRequest, NextApiResponse } from 'next'
import { db } from '../../../database';
import { Entry, IEntry } from '../../../models';

type Data = 
    | { message: string }
    | IEntry[]
    | IEntry

export default function handler (req: NextApiRequest, res: NextApiResponse<Data>) {

    //utilizar switch para saber que tipo de request estamos solicitando a este endpoint
    switch ( req.method ) {
        case 'GET':
                return getEntries(res)
        case 'POST':
            return postEntries(req, res)
        default:
            return res.status(400).json({ message: 'Endpoint no existe' })
    }


}


const getEntries = async ( res: NextApiResponse<Data> ) => {

    await db.connect()

    const entries = await Entry.find().sort({ createdAt: 'ascending' })

    await db.disconnect()

    res.status(200).json(entries)
}



const postEntries = async ( req: NextApiRequest, res:NextApiResponse<Data> ) => {
    //desestructurar unicamente la descripcion que viene del body de la request
    const { description = '' } = req.body;
    const newEntry = new Entry({//creamos un nuevo objeto de tipo Entry
        description,
        createdAt: Date.now(),
    })

    try{
        await db.connect()
        await newEntry.save()//guardamos el nuevo objeto
        await db.disconnect()

        return res.status(201).json( newEntry )//retornamos un estado 201 'creado' y el objeto creado
    }catch(error){
        await db.disconnect()
        console.log(error)
        return res.status(500).json({ message: 'Algo salio mal, revisar consola del servidor' })
    }

    
}