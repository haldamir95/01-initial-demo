import mongoose from 'mongoose';
import type { NextApiRequest, NextApiResponse } from 'next'
import { db } from '../../../database';
import { Entry } from '../../../models';
import { IEntry } from '../../../models/Entry';

type Data = 
    | { message: string }
    | IEntry

export default function handler(req: NextApiRequest, res: NextApiResponse<Data>) {

    const { id } = req.query;
    console.log(id)
    if( !mongoose.isValidObjectId(id) ) {//validar que el id sea un id valido de mongo
        return res.status(400).json({ message: 'El id no es valido '+ id})
    }

    switch( req.method ){
        case 'GET':
            return getEntry(req, res)
        case 'PUT':
            return updateEntry( req, res);
        default:
            res.status(200).json({ message: 'Example' })
    }
   
}


const updateEntry = async (req:NextApiRequest, res:NextApiResponse) => {
    const { id } = req.query //desestructuramos el id que viene de la request
    await db.connect()

    const entryToUpdate = await Entry.findById( id ) //buscamos la Entry en la base de datos

    if(!entryToUpdate){//validamos que la Entry exista en la base de datos
        await db.disconnect()
        return res.status(400).json({message:'No hay entrada con ese ID: '+id})
    }

    //desestructuramos la description y el status del body que viene en el request, 
    //si en dado caso no vienen, utilizamos las que ya esta en la Entry traida de la bD
    const {
        description = entryToUpdate.description,
        status = entryToUpdate.status
    } = req.body

    try{
        //creamos un objeto que va a recibir el resultado de hacer el update
        //el metodo necesita que mandemos el id del objeto que queremos modificar, la informacion a modificar y 
        //agregamos el runValidators para que se revise que el estado sea solo de los estados permitidos
        //y el new para que nos regrese la informacion actualizada
        const updateEntry = await Entry.findByIdAndUpdate( id, {description, status}, {runValidators:true, new:true})
        await db.disconnect()
        res.status(200).json( updateEntry! ) //simbolo de admiracion para jurar que nunca va a ser nulo xD
    }catch(error:any){
        console.log(error)
        await db.disconnect()
        res.status(400).json({message: error.errors.status.message})
    }


}

const getEntry = async (req:NextApiRequest, res:NextApiResponse) => {
    //desestructurar el id del query del metodo get
    const { id } = req.query

    await db.connect()
    const entryInDB = await Entry.findById(id)
    if(!entryInDB){
        await db.disconnect()
        return res.status(400).json({message:'No hay entrada con ese ID: '+id})
    }
    await db.disconnect()
    res.status(200).json(entryInDB)
}