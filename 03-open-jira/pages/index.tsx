import { Card, CardContent, CardHeader, Grid, Typography } from "@mui/material";
import type { NextPage } from "next";
import { Layout } from "../components/layouts";
import { EntryList, NewEntry } from "../components/ui";

const HomePage: NextPage = () => {
  return (
    //El color PRIMARY es el que esta definido en el light-theme.ts
    <Layout title="Home - OpenJira">
      {/* conteiner cambia el comportamiento del grid, y el spacing es el espacio entre sus hijos*/}
      <Grid container spacing={2}>



        <Grid item xs={12} sm={4}>
          {/* sx is like style xtended, toma el View Height  para que ocupe toda la altura de la pantalla*/}
          <Card sx={{ height: "calc(100vh - 100px)" }}>
            <CardHeader title="Pendientes" />
              <NewEntry />
              <EntryList status="pending" />
          </Card>
        </Grid>



        <Grid item xs={12} sm={4}>
          <Card sx={{ height: "calc(100vh - 100px)" }}>
            <CardHeader title="Progreso" />
            <EntryList status="in-progress" />
          </Card>
        </Grid>



        <Grid item xs={12} sm={4}>
          <Card sx={{ height: "calc(100vh - 100px)" }}>
            <CardHeader title="Completadas" />
            <EntryList status="finished" />
          </Card>
        </Grid>



      </Grid>
    </Layout>
  );
};

export default HomePage;
