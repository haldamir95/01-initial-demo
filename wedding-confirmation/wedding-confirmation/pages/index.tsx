import Head from "next/head";
import { Inter } from "@next/font/google";
import CardComponent from "@/components/CardComponent";
import mockRoutes from "@/utils/mockRoutes";
import { GetServerSideProps  } from "next"

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <>
      <Head>
        <title>Guzman Sanchez</title>
        <meta name="description" content="app for confirmate assistance on Guzman Sancehz wedding" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <CardComponent guest={mockRoutes[0]} originalButtonText=''/>
    </>
  );
}


export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const slug = ctx.params?.slug as string;
  const post = null;
  if (!post) {
    return {
      notFound: true, //redirects to 404 page
    };
  }
  return {
    props: {
      post,
    },
  };
};