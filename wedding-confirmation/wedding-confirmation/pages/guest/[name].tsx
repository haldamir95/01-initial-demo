import { NextPage } from "next";
import { GetServerSideProps, GetStaticPaths, GetStaticProps } from "next";
import mockRoutes from "@/utils/mockRoutes";
import { Guest } from "@/interfaces/guest";
import CardComponent from "@/components/CardComponent";
import { supabase } from "../../utils/supebase";

//creamos interfaz Props con el atributo que queremos obtener
interface Props {
  guest: Guest;
  originalButtonText: string;
}

//Creamos el componente que recibe las Props que creamos arriba y desestructuramos el objeto guest
//Este objeto se debe de llamar igual que como lo retornamos en el GetStaticProps
const GuestByNamePage: NextPage<Props> = ({ guest, originalButtonText }) => {
  return (
    <CardComponent guest={guest} originalButtonText={originalButtonText} />
  );
};

// // You should use getStaticPaths if you’re statically pre-rendering pages that use dynamic routes
// export const getStaticPaths: GetStaticPaths = async (ctx) => {

//     //retornar el nombre de todos invitados
//     return {
//         paths: mockRoutes.map(guest => ({ //recorremos el arreglo mockRoutes para que nos devuelva otro arreglo con la estrucutra de params:{ name: ... }
//             params: {
//                 name:guest.name
//             }
//         })),
//         fallback: false
//     }
// }

// export const getStaticProps: GetStaticProps = async (ctx) => {
//     //desestructurar el parametro name del contexto ctx que es el name que viene en la ruta
//     const { name } = ctx.params as { name: string };
//     //utilizar nuestro objeto de mockRoutes para buscar al guest con el name
//     const currentGuest:Guest = mockRoutes.find(guest => guest.name===name)!

//     //Buscando al guest con el name en la base de datos SUPEBASE
//     const result = await supabase
//         .from('confirmations')
//         .select('*')
//         .eq('name',name)

//     //Si esta en la base de datos, devolvemos el resultado de que ya confirmo
//     if(result.status === 200 && result.data?.length){
//         console.log('retorna supebase: ',result.data)
//         return {
//             props: {
//                 guest: result.data
//             },
//         };
//     }console.log('retorna mock: ',currentGuest)
//     //si no esta en la base de datos, devolvemos el objeto encontrado en el mock
//     return {
//         props: {
//             guest: currentGuest
//         },
//     };
// };

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  //desestructurar el parametro name del contexto ctx que es el name que viene en la ruta
  const { name } = ctx.params as { name: string };
  //utilizar nuestro objeto de mockRoutes para buscar al guest con el name
  const currentGuest: Guest = mockRoutes.find((guest) => guest.name === name)!;

  //Buscando al guest con el name en la base de datos SUPEBASE
  const result = await supabase
    .from("confirmations")
    .select("*")
    .eq("name", name);

  //Si esta en la base de datos, devolvemos el resultado de que ya confirmo
  if (result.status === 200 && result.data?.length) {
    let buttonText = "kha";
    if (result.data[0].confirmation === "no") {
      if (result.data[0].persons > 1) {
        buttonText = `Si Asistiremos (${result.data[0].persons})`;
      } else {
        buttonText = "Si Asistire";
      }
    } else if (result.data[0].confirmation === "si") {
      if (result.data[0].persons > 1) {
        buttonText = `Confirmados (${result.data[0].persons})`;
      } else {
        buttonText = "Confirmado";
      }
    }
    return {
      props: {
        guest: result.data[0],
        originalButtonText: buttonText,
      },
    };
  }
  //si no esta en la base de datos, devolvemos el objeto encontrado en el mock
  let buttonText = "kha";
    if (currentGuest.confirmation === "no") {
      if (currentGuest.persons > 1) {
        buttonText = `Si Asistiremos (${currentGuest.persons})`;
      } else {
        buttonText = "Si Asistire";
      }
    } else if (currentGuest.confirmation === "si") {
      if (currentGuest.persons > 1) {
        buttonText = `Confirmados (${currentGuest.persons})`;
      } else {
        buttonText = "Confirmado";
      }
    }
  return {
    props: {
      guest: currentGuest,
      originalButtonText: buttonText
    },
  };
};

export default GuestByNamePage;
