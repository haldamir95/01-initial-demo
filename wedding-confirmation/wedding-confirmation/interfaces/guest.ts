export interface Guest {
    name: string;
    id: number;
    persons:number;
    confirmation:string;
}
