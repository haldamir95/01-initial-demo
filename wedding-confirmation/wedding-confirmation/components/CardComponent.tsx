import { useState, useEffect } from "react";
import { Guest } from "@/interfaces/guest";
import { Grid, Spacer, Card, Col, Row, Button, Text } from "@nextui-org/react";
import { FC } from "react";
import React from "react";
import confetti from "canvas-confetti";
import { supabase } from '../utils/supebase'
import mockRoutes from "@/utils/mockRoutes";


interface Props {
  guest: Guest;
  originalButtonText:string
}



const CardComponent: FC<Props> = ({ guest, originalButtonText }) => {
  const [currentGuest, setGuest] = useState(guest)
  const [buttonText, setButtonText] = useState(originalButtonText)

  useEffect(() => {
    cambiarBoton(currentGuest);
  }, [currentGuest])


  const calculin = () =>{
    let total = 0
  for(let i =0; i<mockRoutes.length;i++){
    total = total + mockRoutes[i].persons
  }
  console.log(total)
  }

  //************************** USANDO SUPABASE */
  const confirmAssistance = async (guest:Guest) =>{
    const result = await supabase
    .from('confirmations')
    .insert({ ...guest, confirmation:'si' })
    .select()

    if(result.status===201 && result.data?.length){
      const newGuest:Guest = {
        id:currentGuest.id,
        name:currentGuest.name,
        persons:currentGuest.persons,
        confirmation:'si'
      }
      setGuest(newGuest)
      
    }else{
      console.log(result)
    }
  }
  //***************************/

  const cambiarBoton = (guest:Guest) => {
    if(guest.confirmation==='no'){
      if(guest.persons>1){
        setButtonText(`S\u00ed Asistiremos (${guest.persons})`)
      }else{
        setButtonText("S\u00ed Asistir\u00e9")
      }
    }else if(guest.confirmation==='si'){
      if(guest.persons>1){
        setButtonText(`Confirmados (${guest.persons})`)
      }else{
        setButtonText("Confirmado")
      }
    }
  }

  //==================CONFETTI
  var count = 200;
  var defaults = {
    origin: { y: 0.6 },
    shapes: ["circle", "square"],
    colors: ["#760101", "#b7b7b7"],
  };
  const fire = async (particleRatio: number, opts: any) => {
    await confetti(
      Object.assign({}, defaults, opts, {
        particleCount: Math.floor(count * particleRatio),
      })
    );
  }
  //------------------------------


  const handlePress = async (guest:Guest) => {
    if(guest.confirmation==='no'){
      confirmAssistance(guest)
    }
    calculin()
    fire(0.25, {
      spread: 26,
      startVelocity: 55,
    });
    fire(0.2, {
      spread: 60,
    });
    fire(0.35, {
      spread: 100,
      decay: 0.91,
      scalar: 0.8,
    });
    fire(0.1, {
      spread: 120,
      startVelocity: 25,
      decay: 0.92,
      scalar: 1.2,
    });
    fire(0.1, {
      spread: 120,
      startVelocity: 45,
    });
  };

  return (
    <Grid.Container gap={2} justify="center">
      <Spacer y={1} />

      {/* ********* GRID FOR DESKTOP ********* */}
      <Grid
        justify="center"
        xl={12}
        lg={12}
        md={12}
        sm={12}
        xs={0}
        style={{ padding: "0px 20px 0px 20px" }}
      >
        <Card css={{ w: "100%", h: "calc(100vh)" }}>
          <Card.Header
            css={{ position: "absolute", zIndex: 1, top: 5, left: 15 }}
          >
            <Col>
              <Text h1 color="white" css={{ fontFamily: "cursive" }}>
                Alan & Melissa
              </Text>
            </Col>
          </Card.Header>

          <Card.Body css={{ p: 0 }}>
            <Card.Image
              src="/large-pic.jpg"
              objectFit="cover"
              width={"100%"}
              height={"100%"}
            />
          </Card.Body>

          <Card.Footer
            isBlurred
            css={{
              position: "absolute",
              bottom: 0,
              zIndex: 1,
              bgBlur: "#0f111466",
              alignItems: "center",
            }}
          >
            <Card css={{ alignItems: "center", bgBlur: "#0f111466" }}>
              <Card.Body>
                <Row>
                  <Col>
                    <Row justify="center">
                      <Text> 1 de Abril </Text>
                    </Row>
                    <Row justify="center">
                      <Text>17:30 horas</Text>
                    </Row>
                  </Col>

                  <Col>
                    <Row justify="center">
                      <Text>Museo de la Merced</Text>
                    </Row>
                    <Row justify="center">
                      <Text>11 Av. 4-49 Zona 1</Text>
                    </Row>
                    
                  </Col>

                  <Col>
                    <Row>
                      <Spacer y={1} />
                    </Row>
                    <Row justify="center">
                      <Button
                        onPress={() =>
                          handlePress(currentGuest)
                        }
                        flat
                        auto
                        rounded
                        css={{ color: "#94f9f0", bg: "#94f9f026" }}
                      >
                        {buttonText}
                      </Button>
                    </Row>
                  </Col>
                </Row>
              </Card.Body>
            </Card>
          </Card.Footer>
        </Card>
      </Grid>



      {/* ********* GRID FOR MOBILE DEVICES ********* */}
      <Grid
        justify="center"
        xl={0}
        lg={0}
        md={0}
        sm={0}
        xs={12}
        style={{ padding: "0 5px 0px 15px" }}
      >
        <Card css={{ w: "100%", h: "100%" }}>
          <Card.Header
            css={{ position: "absolute", zIndex: 1, top: 5, left: 15 }}
          >
            <Col>
              <Text h2 color="white" css={{ fontFamily: "cursive" }}>
                Alan & Melissa
              </Text>
            </Col>
          </Card.Header>

          <Card.Body css={{ p: 0 }}>
            <Card.Image
              src="/device-pic.jpg"
              objectFit="cover"
              width={"100%"}
              height={"100%"}
            />
            <Spacer y={0} />
          </Card.Body>

          <Card.Footer
            isBlurred
            css={{
              position: "absolute",
              bottom: 0,
              zIndex: 1,
              bgBlur: "#0f111466",
              alignItems: "center",
            }}
          >
            <Card css={{ alignItems: "center", bgBlur: "#0f111466" }}>
              <Card.Body>
                <Row>
                  <Col>
                    <Row justify="center">
                      <Text> 1 de Abril </Text>
                    </Row>
                    <Row justify="center">
                      <Text>17:30 horas</Text>
                    </Row>
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <Row justify="center">
                      <Text>Museo de la Merced</Text>
                    </Row>
                    <Row justify="center">
                      <Text>11 Av. 4-49 Zona 1</Text>
                    </Row>
                    
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Row>
                      <Spacer y={1} />
                    </Row>
                    <Row justify="center">
                      <Button
                        onPress={() =>
                          handlePress(currentGuest)
                        }
                        flat
                        auto
                        rounded
                        css={{ color: "#94f9f0", bg: "#94f9f026" }}
                      >
                        {
                          buttonText
                        }
                      </Button>
                    </Row>
                  </Col>
                </Row>
              </Card.Body>
            </Card>
          </Card.Footer>
        </Card>
      </Grid>

    </Grid.Container>
  );
};

export default CardComponent;



