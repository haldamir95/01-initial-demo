import { GetStaticProps, NextPage } from 'next';
import { Pokemon, PokemonListResponse } from '../../interfaces';
import { GetStaticPaths } from 'next'
import { pokeApi } from '../../api';
import { Layout } from '../../components/layouts';
import { Grid, Card, Button, Container, Text, Image } from '@nextui-org/react';
import { getPokemonInfo, localFavorites } from '../../utils';
import { useState } from 'react';
import confetti from 'canvas-confetti';

interface Props {
    pokemon: Pokemon;
}

const PokemonByNamePage:NextPage<Props>= ({pokemon}) => {

    const [isInFavorites, setIsInFavorites] = useState(localFavorites.existInFavorites(pokemon.id));

    const onToggleFavorite = () => {
        //llamamos a la funcion que almacena los favorites en el localstorage y le mandamos el id del prop de pokemon
        localFavorites.toggleFavorite(pokemon.id);
        //ejecutar el seteo en favoritos 
        setIsInFavorites( !isInFavorites );
        //Utilizando el Canvas Confetti
        if( isInFavorites ) return
    
        confetti({
          zIndex:999,
          particleCount:100,
          spread:160,
          angle:-100,
          origin:{
            x:1,
            y:0
          }
        })
      }
   
    return (
        <Layout title={pokemon.name} >
          <Grid.Container css={{ marginTop: "5px" }} gap={2}>
            <Grid>
              <Card isHoverable css={{ padding: "30px" }}>
                <Card.Body>
                  <Card.Image
                    src={
                      pokemon.sprites.other?.dream_world.front_default ||
                      "/no-image.png"
                    }
                    alt={pokemon.name}
                    width="100%"
                    height={200}
                  />
                </Card.Body>
              </Card>
            </Grid>
            <Grid xs={12} sm={8}>
              <Card>
                <Card.Header
                  css={{ display: "flex", justifyContent: "space-between" }}
                >
                  <Text h1 transform="capitalize">
                    {pokemon.name}
                  </Text>
                  <Button 
                    color="gradient" 
                    ghost={ !isInFavorites }
                    onPress={ onToggleFavorite }
                  >
                    { isInFavorites ? 'En Favoritos' : 'Guardar en Favoritos'}
                  </Button>
                </Card.Header>
                <Card.Body>
                  <Text size={30}>Sprites:</Text>
                  <Container display="flex" direction="row" >
                    <Image
                      src={pokemon.sprites.front_default}
                      alt={pokemon.name}
                      width={100}
                      height={100}
                    />
                    <Image
                      src={pokemon.sprites.back_default}
                      alt={pokemon.name}
                      width={100}
                      height={100}
                    />
                    <Image
                      src={pokemon.sprites.front_shiny}
                      alt={pokemon.name}
                      width={100}
                      height={100}
                    />
                    <Image
                      src={pokemon.sprites.back_shiny}
                      alt={pokemon.name}
                      width={100}
                      height={100}
                    />
                  </Container>
                </Card.Body>
              </Card>
            </Grid>
          </Grid.Container>
        </Layout>
      );
}




// You should use getStaticPaths if you’re statically pre-rendering pages that use dynamic routes
export const getStaticPaths: GetStaticPaths = async (ctx) => {
    
    //traer todos los pokemons que puedo tener
    const { data } = await pokeApi.get<PokemonListResponse>('/pokemon?limit=151');
    //retornar el nombre de todos los pokemons
    return {
        paths: data.results.map((pokemon)=>({
            params:{
                name:pokemon.name
            }
        })),
        fallback: "blocking"
    }
}

// You should use getStaticProps when:
//- The data required to render the page is available at build time ahead of a user’s request.
//- The data comes from a headless CMS.
//- The data can be publicly cached (not user-specific).
//- The page must be pre-rendered (for SEO) and be very fast — getStaticProps generates HTML and JSON files, both of which can be cached by a CDN for performance.
export const getStaticProps: GetStaticProps = async (ctx) => {
    
    // //desestructurar el parametro name del contexto ctx
    // const { name } = ctx.params as { name: string };
    // //utilizar la api para obtener la informacion del pokemon por nombre
    // const { data } = await pokeApi.get<Pokemon>(`/pokemon/${name}`);
    // //nuevo objeto pokemon solo con la data importante
    // const pokemon = {
    //     id: data.id,
    //     name: data.name,
    //     sprites: data.sprites
    // };
    
    // return {
    //     props: {
    //         pokemon: pokemon,
    //     },
    // };


    //AHORA UTILIZANDO EL getPokemonInfo
    const { name } = ctx.params as { name: string };
    const pokemon = await getPokemonInfo(name);
    //verificando si existe el pokemon para aplicar Incremental Static Generation
    if( !pokemon ){
      return{
        redirect:{
          destination:'/',
          permanent:false
        }
        
      }
    }
    return {
        props:{
          pokemon: pokemon
        },
        revalidate: 86400 //aplicando Incremental Static Regeneration cada 24 horas
    }
};


export default PokemonByNamePage