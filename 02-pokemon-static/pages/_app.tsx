import '../styles/globals.css'
import type { AppProps } from 'next/app'

import { NextUIProvider } from '@nextui-org/react';
import { darkTheme } from "../themes";


export default function App({ Component, pageProps }: AppProps) {
  return (

    //Se envuelven todos los componentes en el NextUIProvider para que funione la extencion de NextUI
    <NextUIProvider theme={ darkTheme }>
      <Component {...pageProps} />
    </NextUIProvider>
  )
}
