import { Layout } from "../../components/layouts"
import { NoFavorites } from "../../components/ui"
import { useState, useEffect } from "react";
import { localFavorites } from "../../utils";
import { FavoritePokemons } from "../../components/pokemon";


const FavoritesPage = () => {

  //utilizar useState para manejar como un estado del componente los pokemones que estan en la lista de favoritos
  const [favoritePokemons, setFavoritePokemons] = useState<number[]>([]);

  //utilizar useEffect para que se ejecute al momento de iniciar el componente (por eso el vector vacio [])
  //y en el useEffect utilizamos el useState para setear los pokemons que estan en la lista de favoritos
  useEffect(() => {
    setFavoritePokemons(localFavorites.pokemons());
  }, [])
  


  return (
    <Layout title="Pokemons Favoritos">
        
        {
          favoritePokemons.length === 0 ?
          (
            <NoFavorites />
          )
          : 
          (
            <FavoritePokemons pokemons={favoritePokemons}/>
          )
        }
        
    </Layout>
  )
}

export default FavoritesPage