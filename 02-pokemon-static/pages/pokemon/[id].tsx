import { useState } from "react";
import { Layout } from "../../components/layouts";
import { GetStaticProps, NextPage, GetStaticPaths } from "next";
import { pokeApi } from "../../api";
import { Pokemon } from "../../interfaces";
import { Button, Card, Container, Grid, Image, Text } from "@nextui-org/react";
import { localFavorites } from "../../utils";

//al importar esto por primera vez nos dara un error, para solventarlo podemos leer la sugenrencia de solucion
//la cual dice que hay que agregar los tipos de canvas-confetti con el comando
//npm i --save-dev @types/canvas-confetti, nosotros usamos yarn asi que ponemos
//yarn add -D @types/canvas-confetti
import confetti from "canvas-confetti";
import { getPokemonInfo } from '../../utils';

interface Props {
  pokemon: Pokemon;
}

const PokemonPage: NextPage<Props> = ({ pokemon }) => {
  //utilizar un state para saber si el pokemon ya esta en favoritos
  const [isInFavorites, setIsInIfavorites] = useState(localFavorites.existInFavorites(pokemon.id));
  

  const onToggleFavorite = () => {
    //llamamos a la funcion que almacena los favorites en el localstorage y le mandamos el id del prop de pokemon
    localFavorites.toggleFavorite(pokemon.id);
    //ejecutar el seteo en favoritos 
    setIsInIfavorites( !isInFavorites );
    //Utilizando el Canvas Confetti
    if( isInFavorites ) return

    confetti({
      zIndex:999,
      particleCount:100,
      spread:160,
      angle:-100,
      origin:{
        x:1,
        y:0
      }
    })
  }


  //console.log(pokemon);

  return (
    <Layout title={pokemon.name} >
      <Grid.Container css={{ marginTop: "5px" }} gap={2}>
        <Grid>
          <Card isHoverable css={{ padding: "30px" }}>
            <Card.Body>
              <Card.Image
                src={
                  pokemon.sprites.other?.dream_world.front_default ||
                  "/no-image.png"
                }
                alt={pokemon.name}
                width="100%"
                height={200}
              />
            </Card.Body>
          </Card>
        </Grid>
        <Grid xs={12} sm={8}>
          <Card>
            <Card.Header
              css={{ display: "flex", justifyContent: "space-between" }}
            >
              <Text h1 transform="capitalize">
                {pokemon.name}
              </Text>
              <Button 
                color="gradient" 
                ghost={ !isInFavorites }
                onPress={ onToggleFavorite }
              >
                { isInFavorites ? 'En Favoritos' : 'Guardar en Favoritos'}
              </Button>
            </Card.Header>
            <Card.Body>
              <Text size={30}>Sprites:</Text>
              <Container display="flex" direction="row" >
                <Image
                  src={pokemon.sprites.front_default}
                  alt={pokemon.name}
                  width={100}
                  height={100}
                />
                <Image
                  src={pokemon.sprites.back_default}
                  alt={pokemon.name}
                  width={100}
                  height={100}
                />
                <Image
                  src={pokemon.sprites.front_shiny}
                  alt={pokemon.name}
                  width={100}
                  height={100}
                />
                <Image
                  src={pokemon.sprites.back_shiny}
                  alt={pokemon.name}
                  width={100}
                  height={100}
                />
              </Container>
            </Card.Body>
          </Card>
        </Grid>
      </Grid.Container>
    </Layout>
  );
};

// You should use getStaticPaths if you’re statically pre-rendering pages that use dynamic routes
export const getStaticPaths: GetStaticPaths = async (ctx) => {
  const pokemons151: string[] = [...Array(151)].map(
    (value, index) => `${index + 1}`
  );

  return {
    // paths: [
    //     {   //aqui colocamos los parametros que podemos recibir en nuestra pagina actual [id].tsx
    //         params: { id: '1' }
    //         params: { id: '2' }
    //         params: { id: '3' }
    //     }
    // ],
    paths: pokemons151.map((index) => ({
      params: { id: index },
    })),
    fallback: "blocking", //"blocking" si se desea que permita continuar con una pagina fantasma o false si retorna un 404
  };
};

// You should use getStaticProps when:
//- The data required to render the page is available at build time ahead of a user’s request.
//- The data comes from a headless CMS.
//- The data can be publicly cached (not user-specific).
//- The page must be pre-rendered (for SEO) and be very fast — getStaticProps generates HTML and JSON files, both of which can be cached by a CDN for performance.
export const getStaticProps: GetStaticProps = async (ctx) => {
  // //para saber que indice de pokemon estamos buscando en los parametros de la pagina, utilizamos los params del contexto ctx
  // //console.log('parametritos ', ctx.params)
  // //hacemos deconstructoring de los params y le decimos a typescript que el id lo tome como tipo string
  // const { id } = ctx.params as { id: string };

  // const { data } = await pokeApi.get<Pokemon>(`/pokemon/${id}`);
  // //nuevo objeto pokemon solo con la data importante
  // const pokemon = {
  //   id: data.id,
  //   name: data.name,
  //   sprites: data.sprites
  // };
  // return {
  //   props: {
  //     pokemon: pokemon,
  //   },
  // };


  //AHORA UTILIZANDO EL getPokemonInfo
  const { id } = ctx.params as { id: string };
  const pokemon = await getPokemonInfo(id);
  //Validamos si el metodo no devolvio null para implementar el Incremental Static Genration
  if( !pokemon ){
    return {
      redirect:{
        destination: '/',
        permanent: false
      }
    }
  }

  return {
    props:{
      pokemon: pokemon
    },
    revalidate: 86400 //unicamente agregando la propiedad revalidate y un numero en segundos, decimos cada cuanto tiempo se hara el re build de esta pagina
  }
};

export default PokemonPage;
