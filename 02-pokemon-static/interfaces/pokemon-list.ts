/**
 * Para generar automaticamente una interfaz de un JSON orgine, podemos utilizar la app
 * https://app.quicktype.io/ en la cual se pega el JSON y se genera la interfaz.
 * 
 * Pero si se tiene la extension de VSCode Paste json as Code
 * podemos copiar el JSON para tener en el clipboard
 * entrar a las opciones de VSCode con Cmd+Shift+P
 * buscamos Paste JSON as Code y precionamos enter, nos pedira el nombre que le queremos poner a la interfaz
 * y lo creara en el archivo que tengamos abierto. 
 * 
 */
export interface PokemonListResponse {
    count:    number;
    next?:     string;
    previous?: string;
    results:  SmallPokemon[];
}

export interface SmallPokemon {
    name: string;
    url:  string;
    id: number;
    img: string;
}
