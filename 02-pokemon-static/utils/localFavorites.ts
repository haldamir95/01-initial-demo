
const toggleFavorite = ( id:number ) =>{

    let favorites: number[] = JSON.parse( localStorage.getItem(`favorites`) || '[]'); //por si no existe el localStorage.getItem favorites, entonces regresamos un arreglo convertido en string para que JSON.parse lo parsee a un arreglo puro
    
    if(favorites.includes(id)){
        favorites = favorites.filter( pokeId => pokeId !== id ); //retornar arreglo sin el pokemon si en dado existe
    }else{
        favorites.push(id);//si no existe lo insertamos y volvemos a grabar en el local storage
    }
    localStorage.setItem('favorites', JSON.stringify(favorites));
}


//para validar si el pokemon ya existe en la lista y cambiar el boton de agregar en favoritos
const existInFavorites = (id :number):boolean =>{

    if( typeof window === 'undefined' ) return false; 

    const favorites: number[ ] =  JSON.parse( localStorage.getItem(`favorites`) || '[]');

    return favorites.includes(id)

}


//para retornar los pokemons que estan en favoritos
const pokemons = ():number[] =>{

    return JSON.parse(localStorage.getItem('favorites') || '[]')
}


export default {
    existInFavorites,
    toggleFavorite,
    pokemons
}