//axios es un cliente para hacer llamadas a APIS como fetch
import axios from "axios";

//se crea un objeto con el dominio y para las peticiones solo se agrega el resto de la URL
const pokeApi = axios.create({
    baseURL: 'http://pokeapi.co/api/v2'
})

export default pokeApi;

//pokeApi.get('/pokemon?limit=151')