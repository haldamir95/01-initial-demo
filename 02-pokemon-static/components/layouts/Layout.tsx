import Head from "next/head"
import { FC, PropsWithChildren, ReactNode } from "react"
import { Navbar } from '../ui';

//interfaz para establecer el tipo del Functional Component
interface Props{
    title?:string;
    children:ReactNode
}


//Para obtener el URL especifico en el servidor de nuestra imagen del OPEN GRAPH META TAG utilizamos el window.location.origin
const origin = (typeof window === 'undefined') ? '' : window.location.origin;

//Functional Component que funciona como Layout
export const Layout: FC<Props>= ({ children, title}) => {




  return (
    <>
        <Head>
            <title>{title || 'Pokemon App'}</title>
            <meta name="author" content="Alan Guzman"/>
            <meta name="description" content={`Informacion del Pokemon ${title}`}/>
            <meta name="keywords" content={`${title}, pokemon, pokedex`}/>

            {/** Open Graph Meta Tags */}
            <meta property="og:title" content={`Informacion sobre el Pokemon ${title}`}/>
            <meta property="og:description" content={`Esta es la pagina sobre ${title}`}/>
            <meta property="og:image" content={`${origin}/img/banner.png`} />
        </Head>

        <Navbar/>

        <main style={{
            padding: '0px 20px'
        }}>
            {children}
        </main>
    </>
  )
}
