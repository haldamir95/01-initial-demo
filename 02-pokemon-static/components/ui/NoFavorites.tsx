import { Container, Text, Image } from "@nextui-org/react"


export const NoFavorites = () => {
  return (
    <Container
          css={{
            display: 'flex',
            flexDirection:'column',
            height:'calc(100vh - 100px)', //100% del view height - el ancho del navbar
            alignItems:'center',
            justifyContent:'center',
            alignSelf:'center'
          }}
        >
          <Text h1>No hay Favoritos</Text>
          <Image 
            src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/54.svg"
            alt="No hay Favoritos"
            height={250}
            width={250}  
            css={{
              opacity: 0.1
            }}
          ></Image>
        </Container>
  )
}
