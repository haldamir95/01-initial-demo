import { useTheme, Text, Spacer } from "@nextui-org/react";
import Image from "next/image";
import NextLink from "next/link";
import { Link } from "@nextui-org/react";
import { FC } from "react";

//Functional Component para la NavBar
export const Navbar: FC = () => {
  const { theme } = useTheme();

  return (
    //Stilos del componente de navbar
    <div
      style={{
        display: "flex",
        widows: "100%",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "start",
        padding: "0px 20px",
        backgroundColor: theme?.colors.gray100.value,
      }}
    >
      
      {/* Utilizamos el componente Link de NEXT y el Link de NextUI para que el NextLink funcione de enrutador y no nos de el error por pasar Multiples children
          y el Link de NextUI nos ayude a tener unicamente un children dentro del enrutador NextLink

          Es necesario agregar el prop 'legaciBehavior' debido a que en la ultima actualizacion de Next, los hijos de NextLink no pueden ser <a>
          y el componente Link de NextUI es un <a>

          Tambien es buena practica colocar el passHref para que NextLik le herede el href a el <a> tag de <Link> y asi aparezca en el HTML generado.
          Esto es muy util para mejorar el SEO ya que los robots de busqueda de Google pueden navegar mejor por la pagina.

          Se cambio el <Link> por un <div> debido a que seguia tirando errores 
      */}
      <NextLink href={"/"}> 
        <div style={{ display:'flex', alignItems:'center' }}>
          <Image 
            src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/132.png"
            alt="icono de la app"
            width={70}
            height={70}
          />
            <Text color="white" h2>P</Text>
            <Text color="white" h3>okemon!</Text>
        </div>
      </NextLink>

      <Spacer css={{ flex: 1 }} />

      <NextLink href={"/favorites"} >
          <Text color="white">Favoritos</Text>
      </NextLink>
    </div>
  );
};
