import { Grid, Card } from "@nextui-org/react";
import { useRouter } from "next/router";
import { FC } from "react";

interface Props {
  pokemonId: number;
}

export const FavoriteCardPokemon: FC<Props> = ({ pokemonId }) => {

  //Instanciar la funcionalidad de useRouter de Next  
  const router = useRouter();

  //crear una funcion que utiliza useRouter para la navegacion
  const onFavoriteClicked = () => {
    router.push(`/pokemon/${pokemonId}/`);
  };

  return (
    <Grid xs={6} sm={3} md={2} xl={1}>
      <Card
        isHoverable
        isPressable
        css={{ padding: 10 }}
        onPress={onFavoriteClicked} //Agregar la funcionalidad de router al hacer Press sobre el componente Card
      >
        <Card.Image
          src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${pokemonId}.svg`}
          width={`100%`}
          height={140}
        />
      </Card>
    </Grid>
  );
};
